# Cinc Examples

This repository contains some examples of using Cinc Client with test-kitchen.

## Get going now

Using Cinc Client with test kitchen can be done very easily, but relies on deprecated configuration directives. Add this to your kitchen.yml:

```yml
provisioner:
  name: chef_zero
  require_chef_omnibus: true
  chef_omnibus_url: https://omnitruck.cinc.sh/install.sh
  chef_omnibus_root: /opt/cinc
```

And now you're testing with Cinc Client!

## Future proofing

Since the above method relies on deprecated configurations, we also provide an alternative method of using Cinc Client in kitchen. This second method requires a bit more work but uses only forward-compatible methods.

This is the method implemented in this repository.

### Install

First, install ruby either via rvm or whatever method you prefer and then do the following:

``` console
$ bundle install
```

This installs test-kitchen and our fork of mixlib-install which points to our omnitruck instance to ease installation.

### Testing with Cinc Client and Cinc's mixlib-install

Currently, you need to have our fork of mixlib-install installed and the following in your ``kitchen.yml``. Once we have
a Cinc Workstation build made, this repo won't be needed as much.

``` yml
provisioner:
  name: chef_solo
  product_name: cinc
  channel: stable
  chef_omnibus_url: https://omnitruck.cinc.sh/install.sh
  chef_omnibus_root: /opt/cinc
```

Next, just use kitchen like you normally would:
``` console
$ kitchen test default-centos
```
